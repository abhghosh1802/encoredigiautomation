package QATestCase;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import BaseTest.BaseClass;
import BaseTest.BasePage;
import Pages.LoginPage;
import Pages.HomePage;
import Pages.MobileRecharge;
import util.TestUtil;
import util.WebEventListener;
public class TestCase extends BaseClass {
	//LoginPage loginPage;
	//HomePage homePage;
	//MobileRecharge mobileRecharge;
	//public WebDriver driver;
	
	ExtentReports extent;
    ExtentTest test;
	public TestCase(){
		super();
		
	}
	public void loginnew() throws InterruptedException, IOException
	{
		LoginPage loginPage  = new LoginPage();	
		loginPage.LoginClick();
		loginPage.login(prop.getProperty("Username"), prop.getProperty("Password"));
	}
	@BeforeMethod
	public void setUp()
	{
	try 
		{
			extent = new ExtentReports(System.getProperty("user.dir") + "/test-output/ExtentScreenshotNew.html", true);
			initialize();
			
			
			loginnew();
		}
	catch (InterruptedException ex) {
		// TODO Auto-generated catch block
		ex.printStackTrace();
	} 
	catch (IOException ex1) {
		// TODO Auto-generated catch block
		ex1.printStackTrace();
	}
	}
	@Test(priority=2)
	public void loginPageTitleTest() throws IOException{
		HomePage homepagee = new HomePage();
		String title = homepagee.verifyHomePageTitle();
		Assert.assertEquals("RETAILER", title, "The Actual title is "+title +"but the expected is RETAIL");
	}
	@Test(priority=1)
	public void isDisplayedMobileAttibutes( ) throws InterruptedException, IOException
	{
		//loginnew();
		HomePage homepagee = new HomePage();
		homepagee.clickonMobile();
		MobileRecharge mobilerech = new MobileRecharge();
		boolean bol = mobilerech.displaysMobileAttribute();
		Assert.assertEquals(true, bol);
		mobilerech.RechargeMobile("8777056990", "20", "JIO");
		
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException
	{
		 if(result.getStatus() == ITestResult.FAILURE)
	        {
	            String screenShotPath = BasePage.takeScreenshotofStepTest();
	            test.log(LogStatus.FAIL, result.getThrowable());
	            test.log(LogStatus.FAIL, "Snapshot below: " + test.addScreenCapture(screenShotPath));
	            test.log(LogStatus.PASS, "Snapshot below: " + test.addScreenCapture(screenShotPath));
	        }
	        extent.endTest(test);
		
	}
	@AfterTest
    public void endreport()
    {
        driver.close();
        extent.flush();
        extent.close();
    }
	
}
