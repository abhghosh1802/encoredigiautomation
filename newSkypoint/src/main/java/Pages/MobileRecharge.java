package Pages;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ISelect;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import static BaseTest.BasePage.*;
import BaseTest.BasePage;
import BaseTest.BaseClass;
import static BaseTest.BasePage.click;
import static BaseTest.BasePage.enterText;
import static BaseTest.BasePage.refreshPage;
import static BaseTest.BasePage.waitForElem;
import static BaseTest.BasePage.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
public class MobileRecharge extends BaseClass {
	@FindBy(xpath= "//input[@ng-model='mobileRecharge.mobile']")
	public WebElement MobileNumber;
	@FindBy(xpath="//input[@ng-model='mobileRecharge.amount']")
	public WebElement RechargeAmount;
	@FindBy(xpath="//select[@ng-model='mobileRecharge.operator']")
	public WebElement Operator;
	@FindBy(xpath="//body/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/button[1]")
	public WebElement SubmitButton;
	//public WebDriver driver;
	public MobileRecharge() 
	{
		//this.driver = driver;
		//driver=BaseClass.getDriver();
		PageFactory.initElements(driver, this);

	}
	
	public void RechargeMobile(String MobileNumberstr,String Amountstr,String Operatorstr)
	{
		try {
		BasePage.enterText(MobileNumber, MobileNumberstr);
		BasePage.enterText(RechargeAmount, Amountstr);
		Select sel = new Select(Operator);
		sel.selectByVisibleText(Operatorstr);
		BasePage.click(SubmitButton);
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
	}
	public boolean displaysMobileAttribute() throws IOException {
		boolean b = false;
		if(BasePage.isElementPresent(MobileNumber))
		{
		b= true;
		}
		else if(BasePage.isElementPresent(RechargeAmount))
		{
			b=true;
		}
		else if(BasePage.isElementPresent(Operator))
		{
			b=true;
		}
		else if(BasePage.isElementPresent(SubmitButton))
		{
			b=true;
		}
		else
		{
			b= false;
		}
		BasePage.takeScreenshotofStepTest();
		return b;
	}
}


