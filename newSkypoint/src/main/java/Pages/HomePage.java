package Pages;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ISelect;
import org.testng.Assert;

import static BaseTest.BasePage.*;
import BaseTest.BasePage;
import BaseTest.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomePage extends BaseClass {

	@FindBy(xpath= "//ul[@role='tablist']")
	public WebElement MenuItem;
	@FindBy(xpath="(//li[@role='presentation']//a)[1]")
	public WebElement Mobile;
	//public WebDriver driver;
	public HomePage() {
		//driver=BaseClass.getDriver();

		PageFactory.initElements(driver, this);

	}

	public String verifyHomePageTitle() throws IOException {
		String title = driver.getTitle();
		BasePage.takeScreenshotofStepTest();
		return title;
	}
	public void clickonMobile()
	{
		try
		{
			BasePage.click(Mobile);
			//Thread.sleep(3000);
			BasePage.takeScreenshotofStepTest();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
}
