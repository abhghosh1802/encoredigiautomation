package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static BaseTest.BasePage.click;
import static BaseTest.BasePage.enterText;
import static BaseTest.BasePage.refreshPage;
import static BaseTest.BasePage.waitForElem;

import java.io.IOException;

import static BaseTest.BasePage.*;
import BaseTest.BaseClass;
import BaseTest.BasePage;

public class LoginPage extends BaseClass {
	
	@FindBy(xpath= "//input[@ng-model='user.username']")
	public WebElement userId;
	
	@FindBy(xpath= "//input[@ng-model='user.password']")
	public WebElement password;
	@FindBy(xpath= "//button[text()='Login']")
	public WebElement LogInBtn;
	@FindBy(xpath="//a[text()=' LOGIN ']")
	public WebElement LoginButton;
	//public WebDriver driver;
	//BasePage base=new BasePage();
	
	
	public LoginPage() {
		//this.driver = driver;

		PageFactory.initElements(driver, this);
		
		
		
	}
	public void LoginClick() throws IOException
	{
		String pathy = BasePage.takeScreenshotofStepTest();
		click(LoginButton);
	}
	public void login(String user,String pwd) throws InterruptedException  {
		refreshPage();
		waitForElem(20000);
		waitForElem(userId);
		
		boolean isDisplayed=userId.isDisplayed();
		try {
		if (isDisplayed) {
		waitForElem(userId);
		enterText(userId,user);
	    enterText(password,pwd);
		click(LogInBtn);
		waitForElem(40000);
	}else {
		refreshPage();
		waitForElem(userId);
		enterText(userId,user);
	    enterText(password,pwd);
		click(LogInBtn);
		String pathy = BasePage.takeScreenshotofStepTest();
	}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	public String validateLoginPageTitle(){
		return driver.getTitle();
	}
}
