package BaseTest;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import util.TestUtil;



public class BaseClass {
	public static WebDriver driver;
	public static Properties prop;
	public BaseClass(){
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+ "/src/main/resources/Config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*public static WebDriver getDriver()
	{
		if(driver == null)
		{
			return driver;
		}
		else
		{
			return driver;
		}
	}*/
	public static void initialize() {
		String browser=prop.getProperty("Browser");

	       String path=System.getProperty("user.dir");
		//switch(browser.toLowerCase()) {
		if(browser.equalsIgnoreCase("chrome")) {
		//case "chrome":
			

		        System.setProperty("webdriver.chrome.driver", path+"/src/main/resources/Drivers/chromedriver.exe");
		        
		        //Instantiating driver object
		        
		        driver = new ChromeDriver();
		        
		      //  break;
		}
		else if (browser.equalsIgnoreCase("firefox")) {
			
		
		
//	case "firefox":
		

	       
	       System.setProperty("webdriver.gecko.driver", path+"/src/main/resources/Drivers/geckodriver.exe");
	       DesiredCapabilities capabilities = DesiredCapabilities.firefox();
	       capabilities.setCapability("marionette",true);
	     //  driver= new FirefoxDriver(capabilities);
			driver = new FirefoxDriver();
	        
	        //Instantiating driver object
		//	break;
			
		}else
		{
			
	//case "edge":	
			
		
		
		// OperaOptions options = new OperaOptions();
	      //  options.setBinary("C:\\Program Files\\Opera\\56.0.3051.43\\opera.exe");
				//set path to Edge.exe
				System.setProperty("webdriver.edge.driver",path+"/src/main/resources/Drivers/MicrosoftWebDriver.exe");
				//create Edge instance
				driver = new EdgeDriver();
			
		}
		driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.get(prop.getProperty("Url"));
		
	
	
	}
}
